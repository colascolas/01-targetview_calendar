# Demande d'autorisations Android

Ce projet illustre les principes de la demande de permission par une application Android ainsi que les recommandations associées à la gestion d'un dialogue avec l'utilisateur visant à réclamer les autorisations. La page [Wiki](../../wikis/home) documente plus précisémment la démarche. 