package com.example.permissionexample;

import static com.example.permissionexample.CalendarUtility.readCalendarEvent;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Instrumentation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.security.Permission;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private
    RecyclerView recyclerView;
    private Button bt;
    private MyAdapter adapter;
    private ArrayList<String> events = new ArrayList<>();


    /**
     * Launcher de gestion de l'affichage des paramètres de l'application (intention implicite)
     * Est lancé par launch(intent) où intent indique ACTION_APPLICATION_DETAILS_SETTINGS ainsi que la référence de l'appli
     */
    private ActivityResultLauncher<Intent> paramlauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result->{});




    /**
     * Launcher de gestion de la demande de droits
     * Est lancé par launch(nom de la permission)
     */
    private ActivityResultLauncher<String> requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(),
            isGranted->{
                if(isGranted){
                    CalendarUtility.readCalendarEvent(this,events);
                    adapter.notifyDataSetChanged();
                }else{
                    Log.i("Permission: ","Denied");
                    // never ask again checked
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CALENDAR)==false) {
                        showDialog("Quel dommage !", "Vos choix antérieurs placent l'application en mode dégradé. Vous pouvez réactiver l'autorisation dans les paramètres de l'application. ",
                                "Paramètres", (DialogInterface di, int i) -> {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package",getPackageName(),null));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    paramlauncher.launch(intent);
                                    di.dismiss();
                                },
                                "Non merci", (DialogInterface di, int i) -> {
                                    di.dismiss();
                                },
                                false);
                    } else {
                        // demande à nouveau l'autorisation en expliquant pourquoi c'est nécessaire
                        askAgain();

                    }
                }
    });

    private void askAgain(){
        showDialog("Lecture de l'agenda", "La permission de lecture de l'agenda est nécessaire pour récupérer les évènements qui y sont inscrits. ",
                "D'accord", (DialogInterface di,int i)->{di.dismiss();requestPermissionLauncher.launch(Manifest.permission.READ_CALENDAR);},
                "Non merci", (DialogInterface di,int i)->{di.dismiss();},
                false);
    }









    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Initialiser et définir l'adaptateur pour le RecyclerView
        events.add("vide");
        adapter = new MyAdapter(events);
        recyclerView.setAdapter(adapter);

        bt=findViewById(R.id.button);
        bt.setOnClickListener((View view)-> {
                if (readCalendarGranted()) {
                    CalendarUtility.readCalendarEvent(this,events);
                    adapter.notifyDataSetChanged();
                } else {
                    requestPermissionLauncher.launch(Manifest.permission.READ_CALENDAR);
                }
        });


    }


    /**
     * Test de permission
     * @return indique si la permission de lecture calendar est octroyée
     */
    private boolean readCalendarGranted() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED);
    }


    /**
     * Affiche une boite de dialogue
     * @param title
     * @param message
     * @param positiveLabel
     * @param positiveOnClick
     * @param negativeLabel
     * @param negativeOnClick
     * @param isCancelable
     * @return
     */
    private AlertDialog showDialog(String title, String message,
                                   String positiveLabel, DialogInterface.OnClickListener positiveOnClick,
                                   String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
                                   boolean isCancelable){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message).setCancelable(isCancelable);
        builder.setPositiveButton(positiveLabel,positiveOnClick);
        builder.setNegativeButton(negativeLabel,negativeOnClick);
        AlertDialog alert = builder.create();
        alert.show();
        return alert;
    }


    /** Adapter pour le RecyclerView
     *
     * Construit une classe adapter pour le recyclerView
     */
    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

        // référence vers les datas
        private List<String> dataList;

        public MyAdapter(List<String> dataList) {
            this.dataList = dataList;
        }

        /**
         * action accomplie à l'instanciation du ViewHolder
         * @param parent The ViewGroup into which the new View will be added after it is bound to
         *               an adapter position.
         * @param viewType The view type of the new View.
         *
         * @return un ViewHolder contenant des éléments formatés selon item_list
         */
        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
            return new MyViewHolder(view);
        }

        /**
         * action accomplie lorsque des datas doivent être insérées en position position
         * @param holder The ViewHolder which should be updated to represent the contents of the
         *        item at the given position in the data set.
         * @param position The position of the item within the adapter's data set.
         */
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            String data = dataList.get(position);
            holder.textViewItem.setText(data);
        }

        /**
         * Nombre d'éléments dans la liste
         * @return
         */
        @Override
        public int getItemCount() {
            return dataList.size();
        }

        /**
         * Redéfinition du ViewHolder (conteneur individuel des views de la liste)
         * Permet d'optimiser les performances du recyclerView en limitant les réinstanciations lorsque
         * qu'un nouvel item apparait (et qu'un autre disparait).
         */
        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textViewItem;

            /**
             * Le constructeur récupère uniquement la référence vers le texte
             * @param itemView
             */
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                textViewItem = itemView.findViewById(R.id.textViewItem);
            }



        }
    }
}